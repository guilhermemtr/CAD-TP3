#include <unistd.h>
#include <string.h>
#include <stdlib.h>

#include "types.h"
#include "config.h"
#include "server.h"
#include "stack.h"
#include "opt_helper.h"

#ifndef __COMPUTATIONS__
#define __COMPUTATIONS__

void
init_structures();

void
destroy_structures();

void
compute_static(byte* b, unumber* hist, struct execution_info_slave* eis);

void
compute_dynamic(byte* b, unumber* hist, struct execution_info_slave* eis);

void
execute_eval(byte*, unumber* hist, struct execution_info_slave*);

void
execute_run(byte*, unumber* hist, struct execution_info_slave*);

/* tries to fecth work locally or remotely */
boolean
get_work(number* work, unumber execution_mode);


#endif /* __COMPUTATIONS__ */
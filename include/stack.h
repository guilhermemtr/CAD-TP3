#include <stdlib.h>
#include <pthread.h>

#include "types.h"

#ifndef __STACK__
#define __STACK__

struct node {
	void* data;
	struct node* next;
};

struct stack {
	struct node* head;
	pthread_mutex_t sl;
};

/* creates a stack */
struct stack*
create_stack();

/* destroys a stack */
void
destroy_stack(struct stack*);

/* pushes to the stack */
void
push(struct stack*, void*);

/* pops from the stack */
boolean
pop(struct stack*, void**);


#endif /* __STACK__ */
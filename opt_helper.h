#include <stdio.h>
#include <stdlib.h>
#include <argp.h>

#include "mympi.h"
#include "types.h"
#include "config.h"
#include "statistics.h"
#include "mandelbrot.h"
#include "file_utils.h"
#include "logger.h"


#ifndef __OPT_HELPER__
#define __OPT_HELPER__

struct stats_info
{
  boolean rt_avg                    : 1;
  boolean ut_avg                    : 1;
  boolean rt_v                      : 1;
  boolean ut_v                      : 1;
  boolean num_cpus                  : 1;
  boolean cpu_occupation            : 1;
  boolean speed_up                  : 1;
  real    discard;
};

struct execution_info
{
  boolean print_image               : 1;
  char*   image_fn; /* image filename */

  boolean eval                      : 1;
  unumber runs;
  boolean show_total_time           : 1;

  unumber n_threads;

  boolean show_statistics           : 1;

  boolean save_statistics           : 1;
  char*   statistics_fn;

  boolean use_omp                   : 1;

  boolean join_output				        : 1;

  boolean omp_dynamic				        : 1;

  boolean histogram                 : 1;
  char*   histogram_fn;
  boolean histogram_print           : 1;

  unumber execution_mode;

  struct mb_grid data;

  struct stats_info s_i;
};


#define NUM_FIELDS_EXEC_INFO_SLAVE 	10

struct execution_info_slave
{
  unumber join_output;
  unumber print_image;
  char    image_fn[MAX_FILENAME_SIZE];

  unumber execution_type;
  unumber runs;
  unumber n_threads;
  unumber execution_mode;
  unumber histogram;

  unumber dynamic_parallelism;

  struct mb_grid data;
};

/* shows the execution info data */
void
show_exec_info_slave(struct execution_info_slave*);

/* creates a mpi data type for the arguments */
void
create_mpi_execution_info_slave_type();

/* destroys the mpi data type for the arguments */
void
destroy_mpi_execution_info_slave_type();

#endif /* __OPT_HELPER__ */